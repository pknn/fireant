const express = require('express');

const router = express.Router();

const controller = require('../controller/CategoryController');

router.route('/').post(controller.create).get(controller.getAll);

router.route('/:categoryId').get(controller.getById).delete(controller.removeById);

module.exports = router;
