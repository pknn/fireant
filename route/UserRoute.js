
const express = require('express');

const router = express.Router();

const controller = require('../controller/UserController');

router.route('/').post(controller.create).get(controller.getAll);
router.get('/:username', controller.getFromUsername);

module.exports = router;
