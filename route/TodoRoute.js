const express = require('express');

const router = express.Router();

const controller = require('../controller/TodoController');

router.route('/').post(controller.create).get(controller.getAll);

router.route('/:todoId').put(controller.edit).get(controller.getById).delete(controller.delete);

module.exports = router;
