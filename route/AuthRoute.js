
const express = require('express');

const router = express.Router();

const controller = require('../controller/AuthController');

router.route('/login').post(controller.login);
router.route('/logout').post(controller.logout);

module.exports = router;
