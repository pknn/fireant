FROM node:10.12.0-slim
COPY . /fireant
WORKDIR /fireant
RUN npm install --no-optional
CMD npm run production