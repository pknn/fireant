const express = require('express');
const config = require('./config');

require('./service/mongoService');

const host = process.env.HOST || '0.0.0.0';
const port = process.env.PORT || 3000;
let app = express();

app = config(app);
app.listen(port, host);

module.exports = app;
