const bodyParser = require('body-parser');
const cors = require('cors');

const routes = require('./router');

module.exports = (app) => {
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: true,
  }));

  app.use(cors());

  app.use('/api', routes);

  return app;
};
