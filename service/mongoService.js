const mongoose = require('mongoose');

const HOST_URL = process.env.NODE_ENV === 'production' ? 'mongo' : 'localhost';
const PORT = process.env.DB_PORT || 27017;

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${HOST_URL}:${PORT}/fireant`, {
  useNewUrlParser: true,
});

module.exports = {
  mg_: mongoose,
};
