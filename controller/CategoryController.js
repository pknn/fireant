const _ = require('lodash');
const Category = require('../model/Category');
const User = require('../model/User');

module.exports = {
  create: (req, res) => {
    const body = _.pick(req.body, ['name']);
    const { username } = req.headers;
    const token = req.headers['x-auth'];
    const category = new Category(body);

    User.verifyUserAuthToken(username, token).then((user) => {
      if (user) {
        category.user = user;
        category.save().then(() => {
          res.status(201).send();
        }).catch((error) => {
          res.status(400).send(error);
        });
      } else {
        res.status(401).send();
      }
    }).catch((error) => {
      res.status(400).send(error);
    });
  },
  getAll: (req, res) => {
    const { username } = req.headers;
    const token = req.headers['x-auth'];

    User.verifyUserAuthToken(username, token).then((user) => {
      if (user) {
        Category.find({ user }).populate('user', 'username').then((categories) => {
          res.send(categories);
        }).catch((error) => {
          res.status(400).send(error);
        });
      } else {
        res.status(401).send();
      }
    }).catch((error) => {
      res.status(400).send(error);
    });
  },
  getById: (req, res) => {
    const { username } = req.headers;
    const token = req.headers['x-auth'];
    const { categoryId } = req.params;

    User.verifyUserAuthToken(username, token).then((result) => {
      if (result) {
        Category.findById(categoryId).populate('user', 'username').then((category) => {
          res.send(category);
        }).catch((error) => {
          res.status(400).send(error);
        });
      } else {
        res.status(401).send();
      }
    }).catch((error) => {
      res.status(400).send(error);
    });
  },
  removeById: (req, res) => {
    const { username } = req.headers;
    const token = req.headers['x-auth'];
    const { categoryId } = req.params;

    User.verifyUserAuthToken(username, token).then((result) => {
      if (result) {
        Category.findByIdAndRemove(categoryId).then((category) => {
          res.send(category);
        }).catch((error) => {
          res.status(400).send(error);
        });
      } else {
        res.status(401).send();
      }
    }).catch((error) => {
      res.status(400).send(error);
    });
  },
};
