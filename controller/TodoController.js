const _ = require('lodash');

const Todo = require('../model/Todo');
const Category = require('../model/Category');
const User = require('../model/User');

module.exports = {
  create: (req, res) => {
    const body = _.pick(req.body, ['name', 'categoryId', 'dueDate']);
    const { username } = req.headers;
    const token = req.headers['x-auth'];

    const due = new Date(body.dueDate);

    const todo = new Todo({
      ...body, due,
    });

    User.verifyUserAuthToken(username, token).then((user) => {
      if (user) {
        Category.findById(body.categoryId).then((category) => {
          if (category) {
            todo.user = user;
            todo.category = category;
            todo.save().then(() => {
              res.status(201).send();
            }).catch((error) => {
              res.status(500).send(new Error(error));
            });
          } else res.status(404).send();
        }).catch((error) => {
          res.status(500).send(error);
        });
      } else res.status(401).send();
    }).catch((error) => {
      res.status(500).send(new Error(error));
    });
  },

  edit: (req, res) => {
    const body = _.pick(req.body, ['name', 'categoryId', 'due']);
    const { todoId } = req.params;
    const { username } = req.headers;
    const token = req.headers['x-auth'];

    User.verifyUserAuthToken(username, token).then((user) => {
      if (user) {
        Category.findById(body.categoryId).then((category) => {
          if (category) {
            Todo.findByIdAndUpdate(todoId, {
              category, due: body.due,
            }).then(() => {
              res.send();
            }).catch((error) => {
              res.status(500).send(error);
            });
          } else res.status(404).send();
        }).catch((error) => {
          res.status(500).send(error);
        });
      } else res.status(401).send();
    }).catch((error) => {
      res.status(500).send(error);
    });
  },

  getAll: (req, res) => {
    const { username } = req.headers;
    const token = req.headers['x-auth'];

    User.verifyUserAuthToken(username, token).then((user) => {
      if (user) {
        Todo.find({ user }).populate('user', 'username').then((todos) => {
          res.send(todos);
        }).catch((error) => {
          res.status(400).send(error);
        });
      } else {
        res.status(401).send();
      }
    }).catch((error) => {
      res.status(401).send(error);
    });
  },

  getById: (req, res) => {
    const { username } = req.headers;
    const token = req.headers['x-auth'];
    const { todoId } = req.params;

    User.verifyUserAuthToken(username, token).then((user) => {
      if (user) {
        Todo.findById(todoId).populate('user', 'username').then((todo) => {
          res.send(todo);
        }).catch((error) => {
          res.status(400).send(error);
        });
      } else {
        res.status(401).send();
      }
    }).catch((error) => {
      res.status(401).send(error);
    });
  },

  delete: (req, res) => {
    const { username } = req.headers;
    const token = req.headers['x-auth'];
    const { todoId } = req.params;

    User.verifyUserAuthToken(username, token).then((user) => {
      if (user) {
        Todo.findByIdAndRemove(todoId).populate('user', 'username').populate('category').then(() => {
          res.send();
        })
          .catch((error) => {
            res.status(400).send(error);
          });
      } else {
        res.status(401).send();
      }
    }).catch((error) => {
      res.status(401).send(error);
    });
  },
};
