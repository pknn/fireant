const _ = require('lodash');
const User = require('../model/User');

module.exports = {
  create: (req, res) => {
    const body = _.pick(req.body, ['firstName', 'lastName', 'username', 'password']);
    const user = new User(body);
    user.save().then(() => {
      res.status(201).send();
    }).catch((err) => {
      res.status(400).send(err);
    });
  },

  getAll: (req, res) => {
    User.find().then((users) => {
      res.send(users);
    }).catch((err) => {
      res.status(400).send(err);
    });
  },

  getFromUsername: (req, res) => {
    const { username } = req.params;
    User.findOne({ username }).then((user) => {
      if (!user) {
        res.status(404).send();
      } else res.send(user);
    }).catch((err) => {
      res.status(400).send(err);
    });
  },
};
