const _ = require('lodash');
const User = require('../model/User');

module.exports = {
  login: (req, res) => {
    const body = _.pick(req.body, ['username', 'password']);
    User.verifyUserCredential(body.username, body.password).then((user) => {
      user.generateAuthToken().then((token) => {
        res.header('x-auth', token).send();
      }).catch((error) => {
        res.status(500).send(error);
      });
    }).catch((error) => {
      res.status(401).send(error);
    });
  },
  logout: (req, res) => {
    const { username } = req.headers;
    const token = req.headers['x-auth'];

    User.verifyUserAuthToken(username, token).then((user) => {
      if (user) {
        user.removeAuthToken(token).then(() => {
          res.send();
        }).catch((error) => {
          res.status(500).send(error);
        });
      } else {
        res.status(401).send();
      }
    }).catch((error) => {
      res.status(401).send(error);
    });
  },
};
