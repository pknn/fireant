const express = require('express');

const router = express.Router();

const UserRoute = require('./route/UserRoute');
const CategoryRoute = require('./route/CategoryRoute');
const AuthRoute = require('./route/AuthRoute');
const TodoRoute = require('./route/TodoRoute');

router.get('/status', (req, res) => {
  res.send({
    version: '1.0.0',
    status: 'ok',
  });
});

router.use('/users', UserRoute);
router.use('/categories', CategoryRoute);
router.use('/auth', AuthRoute);
router.use('/todos', TodoRoute);

module.exports = router;
