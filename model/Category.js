const mongoose = require('mongoose');

const {
  Schema,
} = mongoose;

const categorySchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: true,
    minlength: 1,
  },
  todos: [{
    type: Schema.Types.ObjectId,
    ref: 'Todo',
  }],
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
});

const Category = mongoose.model('Category', categorySchema);

module.exports = Category;
