/* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */

const mongoose = require('mongoose');

const Category = require('./Category');

const {
  Schema,
} = mongoose;


const todoSchema = new Schema({
  name: {
    type: String,
    trim: false,
    required: true,
  },
  due: {
    type: Schema.Types.Date,
    required: false,
  },
  category: {
    type: Schema.Types.ObjectId,
    ref: 'Category',
  },
  isFinished: {
    type: Boolean,
    required: true,
    default: false,
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
});

todoSchema.post('save', doc => Category.updateOne({ _id: doc.category._id }, {
  $push: {
    todos: this,
  },
}));

todoSchema.post('findOneAndRemove', doc => Category.updateOne({ _id: doc.category._id }, {
  $pull: {
    todos: doc._id,
  },
}));

todoSchema.post('findOneAndUpdate', (doc) => {
  const remove = Category.updateMany({ user: doc.user }, {
    $pull: {
      todos: doc._id,
    },
  });

  const add = Category.updateOne({ _id: doc.category._id }, {
    $push: {
      todos: doc,
    },
  });

  return Promise.all([remove, add]);
});

const Todo = mongoose.model('Todo', todoSchema);

module.exports = Todo;
