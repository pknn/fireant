/* eslint no-underscore-dangle: ["error", { "allow": ["_id"] }] */

const mongoose = require('mongoose');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

const {
  Schema,
} = mongoose;

const userSchema = new Schema({
  firstName: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
  },
  lastName: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
  },
  username: {
    type: String,
    required: true,
    minlength: 1,
    unique: true,
  },
  password: {
    type: String,
    required: true,
    minlength: 8,
  },
  tokens: [{
    access: {
      type: String,
      required: true,
    },
    token: {
      type: String,
      required: true,
    },
  }],
  lastLogin: {
    type: Date,
    default: Date.now,
  },
});

userSchema.methods.toJSON = function secureJSON() {
  const userObject = this.toObject();
  return _.pick(userObject, ['username', 'firstName', 'lastName', '_id']);
};


userSchema.methods.generateAuthToken = function generateAuthToken() {
  const access = 'auth';
  const salt = bcrypt.genSaltSync(10);
  const token = bcrypt.hashSync(this._id.toString(), salt);

  this.tokens = this.tokens.filter(value => value.access !== 'auth');

  this.tokens = {
    access, token,
  };

  return this.save().then(() => token);
};

userSchema.methods.removeAuthToken = function removeAuthToken(token) {
  return this.update({
    $pull: {
      tokens: {
        token,
      },
    },
  });
};


userSchema.statics.verifyUserAuthToken = function verifyUserAuthToken(username, token) {
  return this.findOne({
    username,
  }).then((user) => {
    if (!user) {
      return undefined;
    }

    const authTokenObject = user.tokens.find(value => value.access === 'auth');
    return token === authTokenObject.token ? user : undefined;
  }).catch(() => undefined);
};


userSchema.statics.verifyUserCredential = function verifyUserCredential(username, password) {
  return this.findOne({
    username,
  }).then((user) => {
    if (!user) {
      return Promise.reject(new Error(`Can't find user with ${username} username`));
    }

    return new Promise((resolve, reject) => {
      bcrypt.compare(password, user.password, (error, result) => {
        if (result) {
          resolve(user);
        } else {
          reject(error);
        }
      });
    });
  });
};

userSchema.pre('save', function generateSecurePassword(next) {
  if (this.isModified('password')) {
    bcrypt.genSalt(10, (e, salt) => {
      bcrypt.hash(this.password, salt, (err, hash) => {
        this.password = hash;
        next();
      });
    });
  } else {
    next();
  }
});

const User = mongoose.model('User', userSchema);

module.exports = User;
